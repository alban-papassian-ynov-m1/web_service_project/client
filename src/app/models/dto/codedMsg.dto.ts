export interface CodedMsgDto {
    id?: string;
    label: string;
    algo?: string;
}
