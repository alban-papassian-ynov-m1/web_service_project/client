export interface DecodedMsgDto {
    id: string;
    date: Date;
    codedMsg: string;
    decodedMsg: string;
    userName: string;
}
