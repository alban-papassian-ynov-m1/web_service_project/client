import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SocketService } from './services/socket.service';
import { DecodedMsgService, GetDecodedMsgResponse, GetDecodedMsgsResponse } from './services/decoded.services';
import { CodedMsgService, GetCodedMsgResponse, GetCodedMsgsResponse } from './services/coded.service';
import { AuthService } from './services/auth.service';
import { CodedMsgDto } from './models/dto/codedMsg.dto';
import { DecodedMsgDto } from './models/dto/decodedMsg.dto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild('clodeModal', { static: false }) clodeModal: ElementRef;
  title = 'client';
  user: { username: string, password: string } = { username: '', password: '' };
  newUser: { username: string, password: string } = { username: '', password: '' };
  isLogged: boolean = false;
  username: string;
  socketService = SocketService;
  token: string;
  codedMsgs: CodedMsgDto[];
  decodedMsgs: DecodedMsgDto[];
  isRegister: boolean = false;
  isLogin: boolean = true;
  newCodedMsg: string;

  constructor(
    private codedService: CodedMsgService,
    private decodeService: DecodedMsgService,
    private authService: AuthService,
  ) {
    this.socketService.initSocket();

    if (localStorage.getItem('access_token') && localStorage.getItem('username')) {
      this.isLogged = true;
      this.username = localStorage.getItem('username');
    }

    this.init();
  }

  async init() {
    await this.getAllCodedMsg();
    await this.getAllDecodedMsg();
  }

  login() {
    this.authService.login(this.user.username, this.user.password).subscribe((data) => {
      if (data.success) {
        localStorage.setItem('access_token', data.token);
        localStorage.setItem('username', this.user.username);
        this.token = data.token;
        this.isLogged = true;
        this.username = this.user.username;
      }
    });
  }

  register() {
    this.authService.register(this.newUser.username, this.newUser.password).subscribe((data) => {
      if (data.success) {
        localStorage.setItem('access_token', data.token);
        localStorage.setItem('username', this.newUser.username);
        this.token = data.token;
        this.isLogged = true;
        this.username = this.newUser.username;
      }
    });
  }

  logout() {
    this.isLogged = false;
    this.authService.logout();
    this.token = null;
  }

  getAllCodedMsg() {
    this.codedService.getAll().subscribe((data: GetCodedMsgsResponse) => {
      this.codedMsgs = data.codedMsgs;
    });
  }

  getAllDecodedMsg() {
    this.decodeService.getAll().subscribe((data: GetDecodedMsgsResponse) => {
      this.decodedMsgs = data.decodedMsgs;
    });
  }

  decodMsg(id: string) {
    this.codedService.getOne(id, 'application/javascript', localStorage.getItem('access_token')).subscribe((data: any) => {
      // tslint:disable-next-line: no-eval
      eval(atob(data.codedMsg.algo));
      document.location.reload(true);
    });
  }

  addCodedMsg() {
    const dto: CodedMsgDto = { label: this.newCodedMsg };

    this.codedService.create(dto).subscribe((data: any) => {
      this.clodeModal.nativeElement.click();
      document.location.reload(true);
    });
  }
}
