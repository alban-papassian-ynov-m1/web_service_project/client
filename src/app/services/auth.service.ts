import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Env } from '../environments/env';

export class GenericResponse {
    success: boolean;
    message: string;
    error: any;
    token: string;
    statusCode: number;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    // Http Options
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    constructor(
        private http: HttpClient
    ) { }

    login(username: string, password: string): Observable<GenericResponse> {
        return this.http.post<GenericResponse>(Env.tokenServerURL + '/auth/login', { username, password })
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    register(username: string, password: string): Observable<GenericResponse> {
        console.log("\n\n Log: AuthService -> username", username);
        return this.http.post<GenericResponse>(Env.tokenServerURL + '/auth/register', { username, password })
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    logout() {
        localStorage.removeItem('access_token');
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Errorrrrr Code: ${error.status} \nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }
}
