import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Env } from '../environments/env';
import { DecodedMsgDto } from '../models/dto/decodedMsg.dto';

export interface GetDecodedMsgResponse {
    success: boolean;
    decodedMsg: DecodedMsgDto;
    message: string;
}

export interface GetDecodedMsgsResponse {
    success: boolean;
    decodedMsgs: DecodedMsgDto[];
    totalDecodedMsgsCount: number;
    message: string;
}

@Injectable({
    providedIn: 'root'
})
export class DecodedMsgService {
    constructor(
        private http: HttpClient,
        public router: Router,
    ) { }

    getAll(): Observable<GetDecodedMsgsResponse> {
        return this.http.get<GetDecodedMsgsResponse>(Env.appServerURL + '/decoded-msg')
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Errorrrrr Code: ${error.status} \nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }
}
