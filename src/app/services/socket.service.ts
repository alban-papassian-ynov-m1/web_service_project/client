import * as socketIo from 'socket.io-client';
import { SocketIOClient } from 'socket.io-client';
import { Observable } from 'rxjs';
import { BaseComponent } from '../components/_base.component';

export interface SocketEventPayload {
    date?: Date;
    data: any;
}

// Socket.io events
export enum SocketEvent {
    CONNECT = 'connect',
    DISCONNECT = 'disconnect',
}

export class SocketService extends BaseComponent {

    private static socket: SocketIOClient.Socket;
    private static socketInitialized = false;

    public static initSocket(): void {
        this.socket = socketIo('http://localhost:3002');
        this.socketInitialized = true;
    }

    public static sendEvent(event: string, data?: any): void {
        this.socket.emit(event, data);
    }

    public static setUserId(userId: any): void {
        if (!userId)
            return;
        this.socket.emit('USER ID', userId);
    }

    public static onEvent(evt: SocketEvent): Observable<any> {
        return new Observable<SocketEvent>(observer => {
            this.socket.on(evt, () => observer.next());
        });
    }

    public static onConnected(): Observable<string> {
        if (!this.socketInitialized) {
            return null;
        }
        return new Observable<string>(observer => {
            this.socket.on(SocketEvent.CONNECT, (data: any) => {
                observer.next(data);
            });
        });
    }

    public static onDisconnected(): Observable<string> {
        if (!this.socketInitialized) {
            return null;
        }
        return new Observable<string>(observer => {
            this.socket.on(SocketEvent.DISCONNECT, (data: any) => {
                observer.next(data);
            });
        });
    }

    public static onCustomEvent(evt: string): Observable<SocketEventPayload> {
        if (!this.socketInitialized) {
            console.log(': SocketService -> onCustomEvent - socket not Initialized');
            return null;
        }
        return new Observable<SocketEventPayload>(observer => {
            this.socket.on(evt, (data: any) => {
                observer.next(data);
            });
        });
    }
}
