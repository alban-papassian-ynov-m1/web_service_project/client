import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Env } from '../environments/env';
import { CodedMsgDto } from '../models/dto/codedMsg.dto';

export interface GetCodedMsgResponse {
    success: boolean;
    codedMsg: CodedMsgDto;
    message: string;
}

export interface GetCodedMsgsResponse {
    success: boolean;
    codedMsgs: CodedMsgDto[];
    totalCountCodedMsgs: number;
    message: string;
}

@Injectable({
    providedIn: 'root'
})
export class CodedMsgService {
    constructor(
        private http: HttpClient,
        public router: Router,
    ) {
        //
    }

    getOne(id, language, token): Observable<GetCodedMsgResponse> {
        const headersObject = new HttpHeaders({
            'Content-Type': 'application.json',
            'Authorization': token,
            'Accept': language,
        });
        const httpOptions = {
            headers: headersObject
        };
        return this.http.get<GetCodedMsgResponse>(Env.appServerURL + '/coded-msg/' + id, httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    getAll(): Observable<GetCodedMsgsResponse> {
        return this.http.get<GetCodedMsgsResponse>(Env.appServerURL + '/coded-msg')
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    delete(id) {
        return this.http.delete<GetCodedMsgResponse>(Env.appServerURL + '/coded-msg/' + id)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }


    update(id, codedMsg): Observable<GetCodedMsgResponse> {
        return this.http.put<GetCodedMsgResponse>(Env.appServerURL + '/coded-msg/' + id, JSON.stringify(codedMsg))
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    create(codedMsg): Observable<GetCodedMsgResponse> {
        return this.http.post<GetCodedMsgResponse>(Env.appServerURL + '/coded-msg', codedMsg)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Errorrrrr Code: ${error.status} \nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }
}
