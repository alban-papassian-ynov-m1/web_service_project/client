import { OnDestroy } from '@angular/core';
import { environment } from '../../environments/environment';
export class BaseComponent implements OnDestroy {
    public loading: boolean;

    public errors: string[] = [];

    ngOnDestroy() {
        //
    }
}
